export default {
    Query: {
        users: () => console.log('users Query invoked'),
        user: () => console.log('user Query invoked'),
        me: () => console.log('me Query invoked')
    },
    Mutation: {
        updateUser: () => console.log('updateUser Mutation invoked'),
        deleteUser: () => console.log('deleteUser Mutation invoked')
    }
}