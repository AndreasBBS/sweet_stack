import { GraphQLDateTime } from 'graphql-iso-date';

import userResolvers from './User';

const customScalarResolver = {
    Date: GraphQLDateTime
};

export default [
    customScalarResolver,
    userResolvers
]