import mongoose from 'mongoose';
import isEmail from 'validator/lib/isEmail';
import bcrypt from 'bcrypt';

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        validate: [isEmail, 'No valid email address provided.']
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        maxlength: 42
    },
},
{
    timestamps: true
});

/**
 * Fetch user logging in with email or username
 * @param {string} login - The user's login string input
 * @returns {Promise<User>}
 * TODO: TEST ME!
 */
userSchema.statics.findByLogin = async function(login) {
    return this.findOne({
        username: login
    }).then(user => user? user : this.findOne({
        email: login
    }))
}

// Hash password before saving it to database
userSchema.pre('save', async function(){
    this.password = await bcrypt.hash(this.password, 10);
})

/**
 * Validate password against hashed password
 * @param {string} password - The provided plaintext password
 * @returns {Promise<boolean>}
 * TODO: TEST ME!
 */
userSchema.statics.validatePassword = function(password) {
    return bcrypt.compare(password, this.password);
}

export default mongoose.model('User', userSchema);