# Dockerized React Express GraphQL MongoDB boilerplate

I had to create a Dockerized boilerplate for a fullstack app to be developed. I decided sharing what I did because it might come in handy to someone starting a fullstack project.
It uses Express for the webapp, React for the fronend, GraphQL for the middleman between backend and frontend and MongoDB as database.
It's as simple as I could make it.

To use just clone the repo and run:
```$ docker-compose up```